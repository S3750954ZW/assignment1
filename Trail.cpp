
#include "Trail.h"


Trail::Trail() {
   this -> length = 0;
   this -> positions[0] = nullptr;
}

Trail::~Trail() {
   for (int i = 0; i < this -> length; i++)
   {
      delete this->positions[i];
   }
   
}

Trail::Trail(Trail& other){
   this -> length = other.length;
   for (int i = 0; i < other.length; i++)
   {
      this -> positions[i] = new Position(*(other.getPosition(i)));
   }
};

int Trail::size() {
   return this -> length;
}

Position* Trail::getPosition(int i) {
   return this -> positions[i];
}

void Trail::addCopy(Position* t) {
   this -> positions[this->length] = new Position(*t);
   this -> length++;
}
