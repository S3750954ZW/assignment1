
#include "Position.h"

Position::Position(int x, int y, Orientation orientation) {
   this -> x = x;
   this -> y = y;
   this -> orientation = orientation;
   this -> isStale = false;
}

Position::~Position() {
}

Position::Position(Position& other):x(other.x),y(other.y),orientation(other.orientation){}

int Position::getX() {
   return this -> x;
}

int Position::getY() {
   return this -> y;
}

Orientation Position::getOrientation() {
   return this -> orientation;
}

char Position::getOrienChar() {
   char orienChar = CHAR_NORTH;
   if (this->orientation == ORIEN_NORTH)
   {
      orienChar = CHAR_NORTH;
   }
   else if (this->orientation == ORIEN_EAST)
   {
      orienChar = CHAR_EAST;
   }
   else if (this->orientation == ORIEN_SOUTH)
   {
      orienChar = CHAR_SOUTH;
   }
   else if (this->orientation == ORIEN_WEST)
   {
      orienChar = CHAR_WEST;
   }
   return orienChar;
}

int Position::printProfile(){
   cout<< "X is " << this -> x << endl;
   cout<< "Y is " << this -> y << endl;
   cout<< "Ort is " << this -> getOrienChar() << endl;
   return EXIT_SUCCESS;
}

void Position::setStale(){
   this -> isStale = true;
}

bool Position::posisStale(){
   return this -> isStale;
}

std::string Position::getOrienStr(){
   std::string orienStr = "NORTH";
   if (this->orientation == ORIEN_NORTH)
   {
      orienStr = "NORTH";
   }
   else if (this->orientation == ORIEN_EAST)
   {
      orienStr = "EAST";
   }
   else if (this->orientation == ORIEN_SOUTH)
   {
      orienStr = "SOUTH";
   }
   else if (this->orientation == ORIEN_WEST)
   {
      orienStr = "WEST";
   }
   return orienStr;
}