#include <iostream>
#include <string>

#include "Types.h"
#include "WallFollower.h"

int main(void) {
    Trail* testTrail = new Trail();
    Position* testPosition = new Position(2,2,ORIEN_SOUTH);
    // Position* positionPtrArr[10] = {new Position(0,0,ORIEN_SOUTH)};
    // for (int i = 0; i < 10; i++)
    // {
    //     cout<<"the " << i << "-th element of the array trail is:" << endl;
    //     positionPtrArr[i]->printProfile();
    // }
    // delete []positionPtrArr;
    cout<< "now the size of trail is " << testTrail -> size() << endl;
    cout<< "adding the position to the tail" << endl;
    testPosition -> printProfile();
    testTrail -> addCopy(testPosition);
    delete testPosition;
    cout<< "now the size of trail is " << testTrail -> size() << endl;
    for (int i = 0; i < testTrail -> size(); i++)
    {
        cout<<"the " << i << "-th element of the trail is:" << endl;
        testTrail->getPosition(i)->printProfile();
    }
    delete testTrail;

    WallFollower* testWallFollower = new WallFollower();
    Maze testMaze = {'v'};
    cout<<"the 0-0 element of testMaze is:"<<endl;
    cout<<testMaze[0][0]<<endl;
    testWallFollower ->execute(testMaze);
    cout<<"the robot is at:"<<endl;
    testWallFollower ->getFullPath() ->getPosition(0)->printProfile();
    delete testWallFollower;


}