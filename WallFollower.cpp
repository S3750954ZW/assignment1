
#include "WallFollower.h"


WallFollower::WallFollower() {
   this -> path = new Trail();
}

WallFollower::~WallFollower() {
   if (this -> path != nullptr)
   {
      delete this -> path;
   }
}

void WallFollower::execute(Maze maze) {
   // locate the robot
   int xRobot = 0;
   int yRobot = 0;
   Orientation oRobot = ORIEN_NORTH;
   for (int iY = 0; iY < MAZE_DIM; iY++)
   {
      for (int iX = 0; iX < MAZE_DIM; iX++)
      {
         if (maze[iY][iX] == CHAR_NORTH)
         {
            xRobot = iX;
            yRobot = iY;
            oRobot = ORIEN_NORTH;
         }
         else if (maze[iY][iX] == CHAR_EAST)
         {
            xRobot = iX;
            yRobot = iY;
            oRobot = ORIEN_EAST;
         }
         else if (maze[iY][iX] == CHAR_SOUTH)
         {
            xRobot = iX;
            yRobot = iY;
            oRobot = ORIEN_SOUTH;
         }
         else if (maze[iY][iX] == CHAR_WEST)
         {
            xRobot = iX;
            yRobot = iY;
            oRobot = ORIEN_WEST;
         }
      }
   }
   // locate the goal
   int xGoal = 0;
   int yGoal = 0;
   for (int iY = 0; iY < MAZE_DIM; iY++)
   {
      for (int iX = 0; iX < MAZE_DIM; iX++)
      {
         if (maze[iY][iX] == CHAR_GOAL)
         {
            xGoal = iX;
            yGoal = iY;
         }
         
      }
   }
   // initialise the trail solution with the starting point
   Position* posToAdd = new Position(xRobot,yRobot,oRobot);
   this -> path -> addCopy(posToAdd);
   delete posToAdd;
   // solve the maze with the simplified right-wall following algorithm
   int xCheck=0;
   int yCheck=0;
   // keep solving while the robot is not at the goal regarless of the orientation
   while (xRobot!=xGoal || yRobot!=yGoal)
   {
      xCheck = xRobot;
      yCheck = yRobot;
      // check whether robot's R.H.S. is a wall
      if (oRobot == ORIEN_NORTH)
      {
         xCheck++;
      }
      else if (oRobot == ORIEN_EAST)
      {
         yCheck++;
      }
      else if (oRobot == ORIEN_SOUTH)
      {
         xCheck--;
      }
      else if (oRobot == ORIEN_WEST)
      {
         yCheck--;
      }
      // if R.H.S. is a wall
      if (maze[yCheck][xCheck] == CHAR_WALL)
      {
         // check if the front of the robot is open
         xCheck = xRobot;
         yCheck = yRobot;
         if (oRobot == ORIEN_NORTH)
         {
            yCheck--;
         }
         else if (oRobot == ORIEN_EAST)
         {
            xCheck++;
         }
         else if (oRobot == ORIEN_SOUTH)
         {
            yCheck++;
         }
         else if (oRobot == ORIEN_WEST)
         {
            xCheck--;
         }
         // if the front of the robot is open or GOAL, proceed forward with 1 unit
         if (maze[yCheck][xCheck]==CHAR_OPEN || maze[yCheck][xCheck]==CHAR_GOAL)
         {
            xRobot=xCheck;
            yRobot=yCheck;
            Position* posToAdd = new Position(xRobot,yRobot,oRobot);
            this -> path -> addCopy(posToAdd);
            delete posToAdd;   
         }
         // if the front of the robot is not open, turn left
         else
         {
            if (oRobot == ORIEN_NORTH)
            {
               oRobot=ORIEN_WEST;
            }
            else if (oRobot == ORIEN_EAST)
            {
               oRobot=ORIEN_NORTH;
            }
            else if (oRobot == ORIEN_SOUTH)
            {
               oRobot=ORIEN_EAST;
            }
            else if (oRobot == ORIEN_WEST)
            {
               oRobot=ORIEN_SOUTH;
            }            
            Position* posToAdd = new Position(xRobot,yRobot,oRobot);
            this -> path -> addCopy(posToAdd);
            delete posToAdd;  
         }         
      }
      // if the R.H.S. of the robot is not a wall, turn right
      else
      {
         if (oRobot == ORIEN_NORTH)
         {
            oRobot=ORIEN_EAST;
         }
         else if (oRobot == ORIEN_EAST)
         {
            oRobot=ORIEN_SOUTH;
         }
         else if (oRobot == ORIEN_SOUTH)
         {
            oRobot=ORIEN_WEST;
         }
         else if (oRobot == ORIEN_WEST)
         {
            oRobot=ORIEN_NORTH;
         }
         Position* posToAdd = new Position(xRobot,yRobot,oRobot);
         this -> path -> addCopy(posToAdd);
         delete posToAdd;          
         // and proceed with one unit
         xRobot=xCheck;
         yRobot=yCheck;
         posToAdd = new Position(xRobot,yRobot,oRobot);
         this -> path -> addCopy(posToAdd);
         delete posToAdd; 
      }
   }
   
}

Trail* WallFollower::getFullPath() {
   Trail* resultPath = new Trail(*(this -> path));
   return resultPath;
}

Trail* WallFollower::getEffectivePath() {
   Trail* resultPath = new Trail(*(this -> path));
   for (int i = resultPath -> size()-1; i > 0; i--)
   {
      // check if more than one positions in the full path are next to the current checking position
      int adjacentCount = 0;
      int firstAdjacent = i;
      int lastAdjacent = i;
      for (int p = i; p >= 0; p--)
      {
         int distX = resultPath->getPosition(p)->getX() - resultPath->getPosition(i)->getX();
         int distY = resultPath->getPosition(p)->getY() - resultPath->getPosition(i)->getY();
         if (distX*distX + distY*distY == 1)
         {
            adjacentCount++;
            firstAdjacent = p;
            if (adjacentCount == 1)
            {
               lastAdjacent = p;
            }
            // // for debug purposes
            // std::cout<<"debug"<<std::endl;
            // std::cout<<"i is "<< i <<",p is "<<p<<std::endl;
            // std::cout<<"adjcount is "<< adjacentCount <<",firstAdjacent is "<< firstAdjacent << std::endl;
            // std::cout<<"i pos profile:"<<std::endl;
            // resultPath->getPosition(i)->printProfile();
            // std::cout<<"p pos profile:"<<std::endl;
            // resultPath->getPosition(p)->printProfile();
         }
      }
      // if so then mark stale all positions between the first adjacent position and the current one (excl. for the first)
      if (adjacentCount>1)
      {
         for (int q = firstAdjacent+1; q <= lastAdjacent; q++)
         {
            resultPath->getPosition(q)->setStale();
            // then go to the first adjacent position and repeat the process
            i = q;
            // // for debug purposes
            // std::cout<<"set "<<q<<"-pos stale"<<std::endl;
         }
      }
      // otherwise do nothing and check the next position
   }
   return resultPath;
   
}
