
#ifndef COSC_ASSIGN_ONE_Position
#define COSC_ASSIGN_ONE_Position

#include "Types.h"

//for testing methods DELETE BEFORE SUBMISSION! VVVVV
#include <iostream>
using std::cout;
using std::endl;
//for testing methods DELETE BEFORE SUBMISSION! ^^^^^^

class Position {
public:

   /*                                           */
   /* DO NOT MOFIFY ANY CODE IN THIS SECTION    */
   /*                                           */

   // Constructor/Desctructor
   Position(int x, int y, Orientation orientation);
   ~Position();

   // x-co-ordinate of the particle
   int getX();

   // y-co-ordinate of the particle
   int getY();

   // Orientation the robot is facing
   Orientation getOrientation();

   // Produces a DEEP COPY of the position
   Position(Position& other);   

   // Print profile of a position
   int printProfile();


   /*                                           */
   /* YOU MAY ADD YOUR MODIFICATIONS HERE       */
   /*                                           */
   // Return the character of the orientation
   char getOrienChar();
   void setStale();
   bool posisStale();
   std::string getOrienStr();

private:

   /*                                           */
   /* DO NOT MOFIFY THESE VARIABLES             */
   /*                                           */
   int x;
   int y;
   Orientation orientation;


   /*                                           */
   /* YOU MAY ADD YOUR MODIFICATIONS HERE       */
   /*                                           */
   bool isStale;
};

#endif // COSC_ASSIGN_ONE_Position
